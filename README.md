# Find Entities for which certain crosswalk attributes are missing or not generated#

# Description #

This script is used to find entities for which certain crosswalk attributes are missing or not generated

# How to run script #

* Build project using mvn clean install -U and find jar in target folder or download jar (util-find-missing-crosswalk-attributes.jar) from {{Project}}/jar/ location


* Usage
```
     nohup java -jar -Xmx2G -Xms2G util-find-missing-crosswalk-attributes.jar {{properties-file-path}} > {output-log-filename} 2>&1 &
```
* Properties file
```
   API_URL=https://361.reltio.com/reltio/api/N6QdWQ05wkTZjLA
   ENTITY_TYPE=person
   USERNAME=user@reltio.com
   PASSWORD=*******
   OUTPUT=cleanse_output_02_23.txt
   MAX_COUNT=2000 #count of entites cleansed in a single batch
   SCAN_FILTER=(equals(type,'configuration/entityTypes/HCP'))    

```

* Example:
```
     nohup java -jar -Xmx2G -Xms2G util-find-missing-crosswalk-attributes.jar entity_scan.properties > output_log.txt 2>&1 &
```
